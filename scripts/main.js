width=2*1280;
height=2*720;
seed=Math.random()*30;
stocks=10000000;
offer=1000000;
internal=stocks-offer;
spread=0.03;
asks={};
bids={};
log=[];
log.push(seed);
news=0;
maxNewsFactor=1;
backgroundColor='#000000';
supernews=0;
max=0;
newsRate=0.99;
tradeRate=0.9;
superNewsRate=0.4;
avg=0;
newsRatio=0.5;
function genNumber(){
    number = Math.random()*seed;
    return number;
}

function onLoad(){
    //genBook();
    canvas          = document.getElementById("LatestPrice");
    canvas.width    = width;
    canvas.height   = height;
    ctx             = canvas.getContext("2d");
    ctx.font = "30px Arial";
    console.log(log);
    loadVariables();
    // Start Game
    window.requestAnimationFrame(gameLoop);
}

function genBook(){
    while(offer>0){
        log[Math.floor(Math.random()*offer)];
    }
}
function loadVariables(){
    numTrades           = Number(numTradesValue.value);
    newsRate            = Number(newsRateValue.value);
    superNewsRate       = Number(superNewsRateValue.value);
    newsRatio           = Number(newsRatioValue.value);
    tradeRate           = Number(tradeRateValue.value);
}

function gameLoop(){
    //drawBackground();
    loadVariables();
    
    runTick()
    
    window.requestAnimationFrame(gameLoop);
}

function drawBackground(){
    ctx.fillStyle =  backgroundColor;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
}

function drawLog(){
    max=0;
    ctx.lineWidth = 10;
    for (i=log.length-1;i>(log.length-numTrades) && i>-1;i--){
        if(log[i]>max){max=log[i]}
    }
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    x=width;
    y=height-(height/max)*log[log.length-1];
    //console.log(log[log.length-1]);
    total=0;
    for (i=log.length-1;i>(log.length-numTrades) && i>-1;i--){
        ctx.beginPath();
        ctx.moveTo(x,y);
        x=(width/numTrades)*(numTrades-(log.length-i));
        y=height-(height/max)*log[i];
        ctx.lineTo(x,y);
        if(log[i]<log[i+1]){ctx.strokeStyle = '#00ff00';
        }else{ctx.strokeStyle = '#ff0000';}
        ctx.stroke();
        total=total+log[i];
    }
    //console.log(total);
    ctx.beginPath();
    if(log.length<numTrades){
        avg=total/log.length;
        y=height-(height/max)*avg}
    else{
        avg=total/numTrades;
        y=height-(height/max)*avg}
    ctx.moveTo(0,y)
    ctx.lineTo(width,y);
    ctx.strokeStyle='#ffff00';
    ctx.stroke();
    ctx.fillStyle = "black";
    ctx.fillText(("Last price: $" + log[log.length-1])  , 10, 50);
    ctx.fillText(("Average:   $" + avg)  , 10, 75);


}

function runTick(){
    if(Math.random()>newsRate){
        news=Math.random()-newsRatio;
        if(news>0){
            console.log("Good news everyone!");
            if(news>superNewsRate){
                console.log("Good super news everyone!");
                supernews=log[log.length-1]*2+3
            }
        }else{console.log("Bad news!")}
    }
    if(Math.random()>tradeRate){
        spread=Math.random()
        log.push(log[log.length-1]+(log[log.length-1]*spread*(Math.random()-0.5))+news*log[log.length-1]+supernews);
        drawLog();
        console.log(log[log.length-1]);
        news=0;
        supernews=0;
    }
}

function DisplayChange(newvalue,element) {
    document.getElementById(element).innerHTML = newvalue;
}
